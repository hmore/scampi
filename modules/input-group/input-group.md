Présentation
------------

Le module input-group de Scampi est une copie quasi conforme de son homologue de Bootstrap. Il permet de réunir visuellement les éléments de formulaire et boutons.

Dépendances
-----------

Ce module nécessite l’utilisation des modules buttons et form.


Utilisation
-----------

La présentation des éléments de formulaire passe presque uniquement par la personnalisation des variables et l’ajout de classes sur les éléments html.

Nous vous invitons à vous référer à la documentation spécifique de [Bootstrap concernant les formulaires](http://v4-alpha.getbootstrap.com/components/input-group/).


